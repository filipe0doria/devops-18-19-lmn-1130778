
CA4 -Containers with Docker
========================================

### Step 1 - Create Issues on Bitbucket ###

2.1) Create Issues on BitBucket regarding the the main tasks for CA4:

   On Bitbucket website under issues tab create:
    
    - #22 Create and update README.md regarding Class Assignment CA4
    - #23 Use docker-compose to produce 2 services/containers
    - #24 Publish the images (db and web) to Docker Hub
    - 
### Step 2 - Create a new Branch for CA4 ###

Create a branch locally:

	$ git checkout -b CA4

### Step 3 - Configure Docker ###

Based on https://github.com/atb/docker-compose-demo

As I am running a Windows Home machine, the docker used was the ToolBox version, from [https://docs.docker.com/toolbox/toolbox_install_windows/](https://docs.docker.com/toolbox/toolbox_install_windows/).

The base files from: https://github.com/atb/docker-compose-demo, were copied to the CA4 root folder
    
   - /db
   - /web
   - /docker-compose.yml

### Step 4 - Build a war file ###

On the root of the errai gradle project run the following command to build a war file
(This step is optional as we already had an executable .war file from CA3P2)

    > gradle build   

Copy the generated war file to the CA4 folder:


### Step 5 - Edit the web Dockerfile ###

```

FROM ubuntu

RUN apt-get update && \
  apt-get install -y openjdk-8-jdk-headless && \
  apt-get install unzip -y && \
  apt-get install wget -y

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app/

RUN wget http://download.jboss.org/wildfly/15.0.1.Final/wildfly-15.0.1.Final.tar.gz && \
  tar -xvzf wildfly-15.0.1.Final.tar.gz && \
  sed -i 's#<inet-address value="${jboss\.bind\.address:127\.0\.0\.1}"/>#<any-address/>#g' /usr/src/app/wildfly-15.0.1.Final/standalone/configuration/standalone.xml && \
  sed -i 's#<connection-url>jdbc:h2:mem:test;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE</connection-url>#<connection-url>jdbc:h2:tcp://db:9092//usr/src/data/data;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE</connection-url>#g' /usr/src/app/wildfly-15.0.1.Final/standalone/configuration/standalone.xml && \
  sed -i 's#<password>sa</password>#<password></password>#g' /usr/src/app/wildfly-15.0.1.Final/standalone/configuration/standalone.xml
 
COPY /errai-demonstration-gradle.war /usr/src/app/wildfly-15.0.1.Final/standalone/deployments/errai-demonstration-gradle.war
CMD /usr/src/app/wildfly-15.0.1.Final/bin/standalone.sh

```

The datasource url uses "db" which is the hostname of the container running the h2. The port is the one defined in the db dockerfile for TCP connections.
These lines with the sed update wildfly as in previous CA3P2, in the Vagrantfile.

### Step 6 - The db Dockerfile ###

The Dockerfile for the db image will not have any updates from its base configuration which has all we need, the instalation of jdk, and the setup and run of H2, again, as similar as in CA3P2, with the db Vagrantfile configuration.

```

FROM ubuntu

RUN apt-get update && \
  apt-get install -y openjdk-8-jdk-headless && \
  apt-get install unzip -y && \
  apt-get install wget -y

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app/

RUN wget http://repo2.maven.org/maven2/com/h2database/h2/1.4.199/h2-1.4.199.jar

CMD java -cp ./h2-1.4.199.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists


```


### Step 7 - Docker-compose

Dicker compose yml has the following configuration which defines the future containers names and network ports, and its portforwarding to the host machine.

```
version: '3'
services:
  web:
    build: web
    ports:
      - "8080:8080"
  db:
    build: db
    ports:
      - "8082:8082"
      - "9092:9092"
    volumes:
      - ./data:/usr/src/data

```

To persist data (even when container is destroyed) the volumes line had to be added to the db volume on docker-compose.yml.

7.1) Build the images

To build the docker images based on the previous meantioned configurations.

    > docker-compose build
    
![alt text](img/dockerimages.png "docker_images")  

7.2) Start the containers

    > docker-compose up 


### Step 8 - Testing up

After accessing the web app in http://localhost:8080/errai-demonstration-gradle/ContactListPage and, as contacts were being created, we could see the contacts being persisted in the H2 database http://localhost:8082 (h2:tcp://db:9092//usr/src/data/data)

Once up and running a new folder appeared on the host machine, data, with a data.mv.db file, the h2 database file which was created in the db container and saved in the shared folder between the host machine and the docker container.
   
This was data is persisted and backup even when the containers are shutdown or destroyed.



### Step 9 - Publishing the images (db and web) to Docker Hub ###

9.1) Account 
	hub.docker.com account -> phil1020
	hub.docker.com repository -> phil1020/phil-docker-repo

9.2) Docker images (checking IDs)

![alt text](img/dockerimages.png "docker_images")  

9.3) Docker Hub Login

    > docker login


9.4) Docker Tag Images To repository

	> docker tag f9e01e60c7f4 phil1020/phil-docker-repo
	> docker tag eaa21a04f1a6 phil1020/phil-docker-repo

9.5) Docker Push Image Tags to repository

	> docker push phil1020/phil-docker-repo

 
We can see that both of the images have been pushed successfully:

![alt text](imgs/docker-hub-repo.png "docker-hub-repo")  
 
 
### Step 10 - Commits / Push ###

    > git status 
    > git add .
    > git commit -m "FIX #23 ....."
	> git add Readme.md
    > git commit -m "FIX #22 ....."
    > git push origin CA4
    > git checkout master
    > git merge CA4
    > git push origin master
    
### Step 11 - Tag the Repository ###

    $ git tag -a CA4 -m "CA4"
    $ git push origin ca4
