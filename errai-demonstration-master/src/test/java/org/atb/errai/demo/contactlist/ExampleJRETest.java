package org.atb.errai.demo.contactlist;

import junit.framework.TestCase;
import org.atb.errai.demo.contactlist.client.shared.Contact;
import org.junit.Test;

public class ExampleJRETest extends TestCase {

    @Test
    public void testVoid() {
        Contact contact = new Contact("Filipe", "filipe0doria@gmail.com", "+351961449728");

        String expected = "+351961449728";
        String result = contact.getPhoneNumber();
        assertEquals(expected, result);
    }
}
