package org.atb.errai.demo.contactlist.client.shared;

import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import static org.junit.Assert.*;

public class ContactTest {

    @Test
    @DisplayName("Constructor and getPhoneNumber valid")
    public void testVoid() {
        Contact contact = new Contact("Filipe", "filipe0doria@gmail.com", "+351961449728");

        String expected = "+351961449728";
        String result = contact.getPhoneNumber();
        assertEquals(expected, result);
    }

    @Test
    public void setGetPhoneNumber() {
        Contact contact = new Contact();
        contact.setPhoneNumber("+351961449728");

        String expected = "+351961449728";
        String result = contact.getPhoneNumber();
        assertEquals(expected, result);
    }

    @Test
    public void getId() {
        Contact contact = new Contact(123,"Filipe", "filipe0doria@gmail.com", "+351961449728");

        long expected = 123;
        long result = contact.getId();
        assertEquals(expected, result);
    }

    @Test
    public void setGetId() {
        Contact contact = new Contact();
        contact.setId(1234);

        long expected = 1234;
        long result = contact.getId();
        assertEquals(expected, result);
    }

    @Test
    public void setGetName() {
        Contact contact = new Contact();
        contact.setName("John Doe");

        String expected = "John Doe";
        String result = contact.getName();
        assertEquals(expected, result);
    }

    @Test
    public void setEmail() {
        Contact contact = new Contact();
        contact.setName("john.doe@email.com");

        String expected = "john.doe@email.com";
        String result = contact.getName();
        assertEquals(expected, result);
    }

    @Test
    public void getEmail() {
    }
}