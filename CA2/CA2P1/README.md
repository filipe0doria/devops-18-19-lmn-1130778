
CA2, Part 1 - Build Tools with Gradle
========================================

Step-by-step guide:
------------------------------------


### Step 1 - Update the repository ###

1.1) Creation of Issues on BitBucket regarding the main tasks for CA2:

    - #9 Download and commit the example application to the repository
    - #10 Add a new task to execute the server
    - #11 Add a unit test and update the gradle script so that it is able to execute the test
    - #12 Add a new task of the type Copy to be used to make a backup of the sources of the application
    - #13 Add a new task of the type Zip to be used to make an archive of the sources of the application
    - #14 TAg revision and update README
    
1.2) Example application download and commit to the repository - available at:
    
    https://bitbucket.org/luisnogueira/gradle_basic_demo

### Step 2 - Experiment with the application ###

2.1) Following the instructions available in the readme.md file:

	$ gradlew build
	$ java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001 (run a server on port 59001)
	$ gradlew runClient
	$ gradlew runClient (ran two or more client in different terminals, to see the chat application working)

### Step 3 - Add a new task to execute the server ###

3.1) Update in build.gradle:

    task runServer(type:JavaExec, dependsOn: classes){
        classpath = sourceSets.main.runtimeClasspath
        main = 'basic_demo.ChatServerApp'
        args '59001'
    }

3.2) After editing the build.gradle to add the task, check if it was successfully added:

    $ gradlew tasks --all

3.3) Commit the new changes to git:

    $ git status
    $ git add build.gradle
    $ git commit -m "FIX #10 - ........."
    $ git push -u origin master

### Step 4 - Add a simple unit test and update the gradle script so that it is able to execute the test ###

4.1) Update the gradle dependencies to support junit. To do this update the build.gradle on an IDE and add the following under dependencies:

    dependencies {
         // junit dependency
          testCompile 'junit:junit:4.12'
    }

4.2) Create the folders on the IDE to add the test class:

    ca2/ca2_part1/src/main/test/java/basic_demo/

4.3) Add the following provided unit test:

    @Test
        public void testAppHasAGreeting() {
            App classUnderTest = new App();
            assertNotNull("app should have a greeting", classUnderTest.getGreeting());
        }

4.4) Commit the new changes to git:

    $ git status
    $ git add build.gradle
    $ git add src/main/test/java/basic_demo/AppTest.java
    $ git commit -m "FIX #11 - .............."
    $ git push -u origin master


### Step 5 - Add a new task of type Copy to be used to make a backup of the sources of the application ###

5.1) Create a backup folder to store the src files:
	
	On the selected path:
	$ mkdir backup_src

5.2) Next add a copy task to gradle's task list. Open build.gradle in the IDE again, and add:

    task copySRC(type: Copy) {
        from 'src'
        into 'backup_src'
    }

5.3) After editing the build.gradle to add the task, check if it was successfully added:

    $ gradlew tasks --all

5.4) After checking if the task was on he list, run it to make sure it is working properly: 

    $ gradlew copySRC

The following message should appear on the command line:
    
    "BUILD SUCCESSFUL in 1s
    1 actionable task: 1 executed"

5.5) Commit the new changes to git:

    $ git status
    $ git add build.gradle
    $ git add backup_src
    $ git commit -m "FIX #12 - ........."
    $ git push -u origin master

### Step 6 - Add a new task of type ZIP to be used to make an archive (i.e., zip file) of the sources of the application ###

* Copy and zip all project files.


6.1) Add a ZIP task to gradle's task list. Open build.gradle in the IDE again, and add:


    task zipSRC(type: Zip) {
        from 'src'
        archiveFileName = "mySRCBackup.zip"
        destinationDirectory = file("build/zip")
    }


6.2) After editing the build.gradle to add the task, check if it was successfully added:

    $ gradlew tasks --all


6.3) After checking if the task was on he list, run it to make sure it is working properly: 

    $ gradlew zipSRC

 The following message should appear on the command line:
 
    "  BUILD SUCCESSFUL in 1s
    1 actionable task: 1 executed"


6.4) Check on IDE/File Explorer if the ZIP file was created.

6.5) Commit the new changes to git:

    $ git status
    $ git add build.gradle
    $ git commit -m "FIX #14 - add zip copy"
    $ git push -u origin master


## Actual State


To build a .jar file with the application:

    % gradlew build 

Run the server
--------------
Open another terminal and execute the following gradle task from the project's root directory:

    % gradlew runClient
    
The above task uses task configuration to create a chat server. The server's IP is "localhost" and its port is "59001".

Run a client
------------

Open another terminal and execute the following gradle task from the project's root directory:
The above task assumes the chat server's IP is "localhost" and its port is "59001". If you whish to use other parameters please edit the runClient task in the "build.gradle" file in the project's root directory.


    % gradlew runClient

To run several clients, you just need to open more terminals and repeat the invocation of the runClient gradle task.