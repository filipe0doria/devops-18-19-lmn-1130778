CA2, Part 2 - Build Tools with Gradle
=================================

Step-by-step guide:
------------------------------------

### Step 1 - Create a new branch called gradle-plantuml ###

1.1) Creating the branch

    $ git checkout -b gradle-plantuml

### Step 2 - Update the Repository ###

2.1) Create Issues on BitBucket regarding the main tasks for CA2P2:

    - #15 Create a new branch called gradle-plantuml
    - #16 Copy puml diagrams as described in readme of gradle-plantuml repository
    - #17 Download and commit the repository 
    - #18 Creation of sequence and class diagrams to be used by the render tasks
    - #19 Automate the copy of puml diagrams into the javadoc build folder
    - #20 Merge the gradle-plantuml branch with master a Mark your repository with the tag CA2P2

2.2) Download and commit to the repository the example application available at:
    
    https://github.com/atb/plantuml-gradle

2.3) Select all unversioned files added by the download of the repository:
    
    $ git add ca2_part2
    $ git commit -m "FIX #15 - ..................."
    $ git push origin plantuml-gradle

### Step 3 - Experimentation with the gradle-plantuml application ###

3.1) Execute the task that renders images from the Plantuml diagram specifications:
  
     $ gradlew renderPlantUml

After running the command above, two diagrams (class and sequence diagram) were created on the following path:

     plantuml-gradle-master/build/puml/com/twu/calculator

3.3) Run the task that will generate javadoc documentation:

    $ gradlew javadoc

3.4) Manually copied diagrams previously created to the correct folder so it would show on index.html when opened in the browser:

    copied files from 

    plantuml-gradle-master/build/puml/com/twu/calculator

    to:

    CA2P2/plantuml-gradle-master/build/docs/javadoc/com/twu/calculator

    after refreshing the page, the images were correctly placed and the broken links disappeared

So this means we have an issue, **how could this task be automated in gradle?**

### Step 4 -  Solve the previous issue ###

4.1) To automate the previous task the following task can be implemented on build.gradle:

    task copyDiagrams(type: Copy) {
        from "build/puml/com/twu/calculator"
        into "build/docs/javadoc/com/twu/calculator"
    }

4.2) After editing the build.gradle to add the task, check if it was successfully added, using the following command to display the information of all available tasks:

    $ gradlew tasks --all

4.3) After checking if the task was on the list, run it to make sure it is working properly: 

    $ gradlew copyDiagrams

the following message shall appear on the command line:
    
    "BUILD SUCCESSFUL in 0s
    1 actionable task: 1 executed"

4.4) Committed the file changes and closed the issue on bitbucket

    $ git status 
    $ git add build.gradle
    $ git commit -m "FIX #16 - ...................."
    $ git push origin gradle-plantuml

### Step 5 -  Replicate the solution for the errai-demonstration application ###

5.1) Download the repository (in a folder for Part 2 of CA2) gradle version of errai-demonstration available at:

    https://github.com/atb/errai-demonstration-gradle

5.2) Commit the previous changes

    $ git status
    $ git add errai-demonstration-gradle-master
    $ git commit -m "REF #17 ..........................."
    $ git push origin gradle-plantuml

5.3) Study how gradle is used to build the application (see the readme file)

    5.3.1) Clean

        $ gradlew clean

    5.3.2) Download Wildfly 

        $ gradlew provision

    5.3.3) Build
        $ gradlew build    

    5.4.4) Start Wildfly
        $ gradlew startWildfly

    5.4.5) Access the application through the following url:
	    http://localhost:8080/errai-demonstration-gradle/ContactListPage    

5.4) **Update gradle to add plantuml and javadoc** (similarly to the gradle-plantuml project)

5.4.1) Run gradle to generate javadoc:
    
    $ gradlew javadoc

5.4.2) To render the plantuml, first add the following task to the build.gradle:

        task renderPlantUml(type: RenderPlantUmlTask) {
    }

Manually copied the directories from the plant-uml-gradle-master project to the errai-demonstration-master-project:

    plantuml-gradle-master/buildSrc

to:

    CA2P2/buildSrc

After this, there was the need to also create the following directory on the errai project:

	CA2P2/src/main/puml


Afterwards the Class and Sequence Diagrams were updated to represent a "get all contacts" interaction on the Errai application on:

    CA2P2/src/main/puml

Finally, run the task on the command line:

    $ gradlew renderPlantUml

The following message appeared on the terminal:

    BUILD SUCCESSFUL in 0s
    1 actionable task: 1 executed

And 2 images files could be found on:

    CA2P2/build/puml
![Sequence Diagram](./sequence-diagram.png)
![Class Diagram](./class-diagram.png)

5.4.3) Run the task that will generate javadoc documentation:

    $ gradlew javadoc
    
 The following message appeared on the terminal:

    BUILD SUCCESSFUL in 0s
    3 actionable tasks: 3 up-to-date

5.4.4) Copy overview.html from gradle-plantuml project to a new folder on the errai project:

Created folder:

    CA2P2/src/main/javadoc

Added overview.html to folder.

Edited overview.html file to reflect the information relative to errai project instead of gradle-plantuml project (edited text and diagrams path)

5.4.5) Edit the build.gradle javadoc task to support the overview
   
    javadoc {
        // The following settings are required for GWT projects
        source = sourceSets.main.allJava
        options.addStringOption("sourcepath", "")
        options.overview = "src/main/javadoc/overview.html" // relative to source root

    }

5.4.6) Add a copyDiagrams task to build.gradle 

    task copyDiagrams(type: Copy) {
        from "build/puml"
        into "build/docs/javadoc/org/atb/errai/demo/contactlist/server"
    }

5.4.7) Run Copy Task
    
     $ gradlew/copyDiagrams

5.4.8) Run Javadoc Task
    
    $ gradlew javadoc

5.4.9) Open the index.html on a browser and see the new javadoc information and diagrams

### Optional way, and more efficient
5.5) Instead of copying the files to a secundary folder for the javadoc runtime to get, we can update the overview.html file to get the diagram files from its renderization folder, /build/puml. To do this we simply need to update the files source, as in:

    <h2>Class Diagram</h2>  
    <img src="../../../build/puml/class-diagram.png"></a><br/>  
      
    <h2>Sequence Diagram</h2>  
    <img src="../../../build/puml/sequence-diagram.png"></a><br/> 

This way only the renderization class is needed. And given so we just need to run:

	$ gradlew renderPlantUml 
	
to render the puml files to png images and then,

	$ gradlew javadoc
to compile the project source files and the overview.html.


### Closing up and updating
6.1) Commit changes to bitbucket and close issue

    $ git status
    $ git add .
    $ git commit -m "FIX #19 - Automate the copy of puml diagrams into the javadoc build folder"
    $ git push origin gradle-plantuml

6.2) Replace README.md text with the step-by-step guide from ca2-part2 and commit to git

    $ git status
    $ git add README.md
    $ git commit -m "add readme.md"
    $ git push origin gradle-plantuml

6.3) Merge the gradle-plantuml branch with master

    $ git checkout master
    $ git merge gradle-plantuml
    $ git push origin master

6.4) Mark the repository with the tag CA2P2.

    $ git tag -a CA2P2 -m "CA2 assignment part 2"
    $ git push origin CA2P2 
