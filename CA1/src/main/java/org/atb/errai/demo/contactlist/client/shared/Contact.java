package org.atb.errai.demo.contactlist.client.shared;

import org.jboss.errai.common.client.api.annotations.Portable;
import org.jboss.errai.databinding.client.api.Bindable;

import javax.persistence.*;
import java.io.Serializable;

@Bindable
@Portable
@Entity
@NamedQueries({@NamedQuery(name = Contact.ALL_CONTACTS_QUERY, query = "SELECT c FROM Contact c ORDER BY c.id")})
public class Contact implements Serializable, Comparable<Contact> {
    public static final String ALL_CONTACTS_QUERY = "allContacts";
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    private String name;
    private String email;
    private int phone;

    public Contact() {
    }

    public Contact(String name, String email, int phone) {
        this.setName(name);
        this.setEmail(email);
        this.setPhone(phone);
    }

    public Contact(long id, String name, String email, int phone) {
        this(name, email, phone);
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        name.trim();
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        if (email.trim().matches("^[\\w!#$%&’*+/=?`{|}~^-]+(?:\\.[\\w!#$%&’*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$"))
            this.email = email;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Contact [id=" + id + ", name=" + name + ", email=" + email + ", phone=" + phone + "]";
    }

    @Override
    public int compareTo(Contact contact) {
        return (int) (id - contact.id);
    }

    @Override
    public boolean equals(final Object obj) {
        return (obj instanceof Contact) && ((Contact) obj).getId() != 0 && ((Contact) obj).getId() == getId();
    }

    @Override
    public int hashCode() {
        return (int) getId();
    }

    public boolean isValid() {
        if (this.phone == 0)
            return false;
        if (this.email == null)
            return false;
        return this.name != null;
    }
}
