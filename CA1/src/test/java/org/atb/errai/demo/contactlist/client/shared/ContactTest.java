package org.atb.errai.demo.contactlist.client.shared;

import org.junit.Assert;
import org.junit.Test;

public class ContactTest {


    @Test
    public void testAttributesAreValid() {
        Contact c = new Contact();
        Assert.assertNotNull(c);
    }

    @Test
    public void testAttributesAreValid2() {
        Contact c = new Contact("phil", "phil@email", 999999999);
        Assert.assertNotNull(c);
    }

    @Test
    public void testAttributesAreValid3() {
        Contact c = new Contact(1L, "phil", "phil@email", 999999999);
        Assert.assertNotNull(c);
    }

    @Test
    public void getId() {
        Contact test = new Contact(1L, "phil", "phil@email", 999999999);
        long id = test.getId();
        Assert.assertEquals(1L, id);
    }

    @Test
    public void setId() {
        Contact test = new Contact("phil", "phil@email", 999999999);
        test.setId(1L);

        long id = test.getId();
        Assert.assertEquals(1L, id);
        Assert.assertNotNull(id);
    }

    @Test
    public void getName1() {
        Contact test = new Contact(1L, "phil", "phil@email", 999999999);
        String name = test.getName();
        Assert.assertEquals("phil", name);
    }

    @Test
    public void setName1() {
        Contact test = new Contact();
        test.setName("phil");

        String name = test.getName();
        Assert.assertEquals("phil", name);
    }

    //FIXME commented since validation is not functional
    //@Test
    public void setName2() {
        Contact test = new Contact();
        test.setName("phil12123");

        String name = test.getName();
        Assert.assertNull(name);
    }

    @Test
    public void getEmail() {
        Contact test = new Contact(1L, "phil", "phil@email.com", 999999999);

        String email = test.getEmail();

        Assert.assertEquals("phil@email.com", email);
    }

    @Test
    public void setEmail() {
        Contact test = new Contact();

        test.setEmail("phil@email.com");

        String email = test.getEmail();
        Assert.assertEquals("phil@email.com", email);
    }

    @Test
    public void setEmail2() {
        Contact test = new Contact();

        test.setEmail("phil@email");

        String email = test.getEmail();
        Assert.assertNull(email);
    }

    @Test
    public void getPhone() {
        Contact test = new Contact(1L, "phil", "phil@email", 999999999);

        int phone = test.getPhone();
        Assert.assertEquals(999999999, phone);
    }

    @Test
    public void setPhone() {
        Contact test = new Contact();

        test.setPhone(999999999);

        int phone = test.getPhone();
        Assert.assertEquals(999999999, phone);
    }

    @Test
    public void toString1() {
        Contact test = new Contact(1L, "phil", "phil@email.com", 999999999);

        String toString = test.toString();
        String expected = "Contact [id=" + 1L + ", name=" + "phil" + ", email=" + "phil@email.com" + ", phone=" + 999999999 + "]";
        Assert.assertEquals(expected, toString);
    }

    @Test
    public void compareTo() {
        Contact original = new Contact(1L, "phil", "phil@email", 999999999);
        Contact test = new Contact(2L, "john", "john@email", 111111111);

        Assert.assertEquals(1, test.compareTo(original));
    }

    @Test
    public void compareTo2() {
        Contact original = new Contact(1L, "phil", "phil@email", 999999999);

        Assert.assertEquals(0, original.compareTo(original));
    }

    @Test
    public void equals1() {
        Contact test = new Contact(2L, "john", "john@email", 111111111);

        Assert.assertTrue(test.equals(test));

    }

    @Test
    public void equals2() {
        Contact original = new Contact(1L, "phil", "phil@email", 999999999);
        Contact test = new Contact(2L, "john", "john@email", 111111111);

        Assert.assertFalse(test.equals(original));

    }

    @Test
    public void hashCode1() {
        Contact test = new Contact(2L, "john", "john@email", 111111111);
        long id = test.hashCode();
        Assert.assertEquals(2L, id);
    }

    @Test
    public void isValid() {
        Contact test = new Contact();
        test.setPhone(999999999);
        test.setEmail("phil@email.pt");
        test.setName("phil");

        Assert.assertTrue(test.isValid());
    }

    @Test
    public void NameIsNotValid() {
        Contact test = new Contact();
        test.setPhone(999999999);
        test.setEmail("phil@email");

        Assert.assertFalse(test.isValid());
    }

    @Test
    public void EmailIsNotValid() {
        Contact test = new Contact();
        test.setPhone(999999999);
        test.setName("phil");

        Assert.assertFalse(test.isValid());
    }

    @Test
    public void EmailIsNotValid2() {
        Contact test = new Contact();
        test.setPhone(999999999);
        test.setName("phil");
        test.setEmail("phil@email");

        Assert.assertFalse(test.isValid());
    }

    @Test
    public void PhoneIsNotValid() {
        Contact test = new Contact();
        test.setName("phil");
        test.setEmail("phil@email");

        Assert.assertFalse(test.isValid());
    }
}