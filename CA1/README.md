
CA1 - Version Control with Git
========================================

**Description:** This is the assignment regarding the Version Control with Git topic.

Step-by-step guide:
------------------------------------
After setting up a new repository and creating a folder for the CA1, the base project errai-demonstration was cloned into it and marked with a tag v1.2.0.


### Step 1 - Developing a new feature ###

1.1) Creating the issue on the repository

1.2) Creating the branch for the new feature to be developed on

	$ git checkout -b phone-field

1.3) Add support for a phone field, updating respective classes:
	
	- Contact.java
	- ContactDisplay.java
	- ContactListPage.html
	- NewContactPage.html
	- NewContactPage.java

### Step 2 - Uploading changes ###

2.1) Commit changes that support phone field implementation:

Check unstaged files:	

    $ git status
	$ git commit -m "FIX #2 - added phone field feature
	$ git push origin phone-field

2.2) Add unit tests for testing the creation of contacts and its attributes:
	
	- Created test class "ContactTest"
	- adds unit tests, coverage at 100%

Commit changes:

Check unstaged files
 
    $ git status
	
Add test files and commit to GIT:

    $ git add .
	$ git commit -m "REF #3 - add unit tests"
	$ git push origin phone-field
	
### Step 3 - Merging branches ###

3.1) Merge the code with Master

	$ git checkout master
	$ git merge phone-field

3.2) Create a new Tag and push it to GIT

	$ git tag -a v1.3.0 -m "my version 1.3.0"
	$ git push origin v1.3.0
	
### Step 4 - Fixing bugs ###
4.1) Create a branch "fix-invalid-email"
    
    $ git checkout -b fix-invalid-email

4.2) Develop fix and test it

4.3) Commit

	$ git commit -m "REF #6 Add email input validation"
	$ git push origin fix-invalid-email

4.4) Merge and Tag application stable version 

	$ git checkout master
	$ git merge fix-invalid-email


### Step 5 - Build & Execution ###

To execute the application while in development you should use the GWT's dev mode.  
  
You do that by executing in the folder of the project:  
  
 % mvn gwt:run
 
 
##  Alternatives - OpenXava
Develop AJAX web applications writing just Java code, without writing HTML, JavaScript or CSS. Like GWT, but OpenXava is designed to be very productive for business applications.

Write just the domain classes in plain Java. Get a web application ready for production.

OpenXava uses a model-driven approach to rapid development, in which you produce a model and obtain a full application from it. The distinguishing feature of OpenXava is that the model consists of a Business Component.
The Business Component approach allows you to structure the application around business concepts. In OpenXava a plain annotated Java class defines the Business Component, making application development highly declarative.
Apart from business components an OpenXava application has modules, controllers, validators, calculators, etc. that you can optionally use to customize your application. You can even customize the way OpenXava generates the user interface using editors and custom views.

OpenXava extends the capacities of JUnit, allowing you to test an OpenXava module exactly in the same way an end user would. In fact, OpenXava uses HtmlUnit, software that simulates a real browser (including JavaScript) from Java. All this is available from the OpenXava class ModuleTestBase. It allows you to automate the test you would do by hand using a real browser in a simple way.