CA5 Part 2 - CI/CD Pipelines with Jenkins
===================

Continuous Integration: Jenkins
----------------
“Continuous integration (CI) is the practice of merging all developer working copies to a
shared mainline several times a day.” - https://www.thoughtworks.com/continuous-integration

"Jenkins is an open source automation server which enables developers around the world to reliably build, test, and deploy their software." - https://jenkins.io/


Description of the assignment
----------------

The goal of the second part of the assignment is to create a pipeline using a Jenkinsfile with:

Checkout. Checkout the code from the repository.
Assemble. Compile and Produce the archive files with the application.
Test. Execute Unit Tests and publish them in Jenkins the Test results.
Javadoc. Generate the javadoc (including Plantuml diagrams) and publish them in Jenkins.
Archive. Archive in Jenkins the archive files (generated during Assemble).
Docker Image. Generate a docker image with Wildfly and the war file and publish it in the Docker Hub.

### Step 1 - Set Up a new Job ###

A new job of type pipeline will run the project from CA2P2, using the Jenkinsfile available in the SCM.

### Step 2 - Set Up Jenkinsfile ###

```
pipeline {
        agent any

        stages {
            stage('Checkout') {
                steps {
                    echo 'Checking out...'
                    git credentialsId: 'bitbucket-credentials', url: 'https://bitbucket.org/filipe0doria/devops-18-19-lmn-1130778/src/master/CA2/CA2P2/errai-demonstration-master'
                }
            }
            stage('Assemble') {
                steps {
                    echo 'Assembling...'
                    bat 'gradle clean assemble'
                }
            }
            stage('Test') {
                steps {
                    echo 'Testing...'
                    bat 'gradle test'
                    junit '**\\test-results\\test\\*.xml'
                }
               }
            stage('Javadoc') {
                steps {
                    echo 'Publishing...'
                    bat 'gradle renderPlantUml'
                    bat 'gradle javadoc'
                    bat 'gradle copyUmlToDocs'
                    publishHTML([
                        allowMissing: false,
                        alwaysLinkToLastBuild: false,
                        keepAll: true,
                        reportDir: 'build/docs/javadoc/',
                        reportFiles: 'index.html',
                        reportName: 'Javadoc',
                        reportTitles: ''])
                }
            }
            stage('Archiving') {
                steps {
                    echo 'Archiving...'
                    archiveArtifacts 'build/libs/*'
                }
            }
            stage('Docker Image') {
                steps {
                    echo 'Publishing...'
                    script {
                        docker.withRegistry('https://hub.docker.com/', 'dockerid') {
                            def customImage = docker.build("phil1020/phil-docker-repo:${env.BUILD_ID}")
                            customImage.push()
                            }
                        }
                }
            }
        }
    }
```

### Step 3 - Set Up Dockerfile ###
```
FROM ubuntu

RUN apt-get update && \
  apt-get install -y openjdk-8-jdk-headless && \
  apt-get install unzip -y && \
  apt-get install wget -y

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app/

RUN wget http://download.jboss.org/wildfly/15.0.1.Final/wildfly-15.0.1.Final.tar.gz && \
  tar -xvzf wildfly-15.0.1.Final.tar.gz && \
  sed -i 's#<inet-address value="${jboss\.bind\.address:127\.0\.0\.1}"/>#<any-address/>#g' /usr/src/app/wildfly-15.0.1.Final/standalone/configuration/standalone.xml

COPY build/libs/errai-demonstration-gradle-master.war /usr/src/app/wildfly-15.0.1.Final/standalone/deployments/errai-demonstration-gradle-master.war

CMD /usr/src/app/wildfly-15.0.1.Final/bin/standalone.sh
```

### Step 4 - Launch Docker
Before running the Job, launch the Docker Toolbox. The Docker needs to be working in order to create a Docker image from a Dockerfile in the repository during the Pipeline run.

### Step 5 - Execute the Job
Selecting "Build Now" to execute the pipeline. When the pipeline build is sucessfull on all steps we have a docker new docker image on the docker hub repository.