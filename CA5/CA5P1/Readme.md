CA5 Part 1 - CI/CD Pipelines with Jenkins
===================

Continuous Integration: Jenkins
----------------
“Continuous integration (CI) is the practice of merging all developer working copies to a
shared mainline several times a day.” - https://www.thoughtworks.com/continuous-integration

"Jenkins is an open source automation server which enables developers around the world to reliably build, test, and deploy their software." - https://jenkins.io/


Description of the assignment
----------------

To create a pipeline using a Jenkinsfile with:

- Checkout. To checkout the code form the repository.
- Assemble. To compile and produce the archive files with the application.
- Test. To execute the Unit Tests and publish the Test results in Jenkins.
- Archive. To archive the previous generated archive files in Jenkins.


### Step 1 - Install and configure jenkins ###

	The used jenkins version was the 2.176.1 for Windows (.msi version), downloaded from https://jenkins.io/download/.
	
	After installing the http://localhost:8080 was prompted to finalize the jenkins installation.
	
	The InitialAdminPassword was stored under C:\Program Files (x86)\Jenkins\secrets.
	
	The default plugins installation method was used, since it was everything we will need to create the CI pipeline.
	
	After the installation of the plugins we can see the Dashboard were the jobs will be displayed. 

### Step 2 - Create job & pipeline ###

The following steps must be performed to accomplish this assignment:

3.1) Jenkins: create new credentials key (this also can also be done when creating a new Job) to be used in the Jenkinsfile;

![Image](image1.jpg)


3.2) Add a Jenkinsfile with the required stages;

added to the "gradle basic demo" project:

pipeline {
            agent any

                stages {
                    stage('Checkout') {
                        steps {
                            echo 'Checking out...'
                            git credentialsId: 'bitbucket-credentials', url: 'https://filipe0doria@bitbucket.org/filipe0doria/devops-18-19-lmn-1130778/CA2/CA2P1'
                        }
                    }
                    stage('Assemble') {
                        steps {
                            echo 'Assembling...'
                            dir('gradle_basic_demo') {
    				        bat 'gradle clean assemble'
                            }
                        }
                    }
                    stage('Test') {
                        steps {
                            echo 'Testing...'
			                dir('gradle_basic_demo') {
                            bat 'gradle testClasses'
							junit '**\\test-results\\test\\*.xml'
                            }
                        }
                    }
                    stage('Archiving') {
                        steps {
                            echo 'Archiving...'
			                dir('gradle_basic_demo') {
                            archiveArtifacts 'build/distributions/*'
                            }
                        }
                    }
                }
        }
	}
	
	- 1. Checkout - meant to checkout the code from Bitbucket repository. The ID of the credentials that were previously created on Jenkins for Bitbucket was used to grant access to the repository through its URL. 
	- 2. Assemble - to compile and produce the executable file.
	- 3. TestClasses - to execute the unit tests and publish them in Jenkins.
	- 4. Archive - which saves the files generated during the Assemble stage in the distributions directory.

3.3) Jenkins: create a new Job to execute the pipeline (can be done first);
	 
	 Scroll to the "Pipeline" section and select "Pipeline script from SCM";
	 Insert the "Repository URL", the "Credentials" that were previously created and the "Script Path", as shown here:

![Image](image3.jpg)

	The pipeline is now created and ready to build.