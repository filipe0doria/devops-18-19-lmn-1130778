# CA3, Part 1 - Virtualization with Vagrant

## Step-by-step guide:

### Step 1 - Create VM

The virtual machine was created following the steps learned on the class;

### Step 2 - Clone the Repository

2.1) Clone the individual repository inside the VM:

    $ git clone ...........

### Step 3 - Build and Execute all Projects

3.1) Since some projects need maven to build, install maven on the VM:

    $ sudo apt install maven

#### 3.2) Build and execute CA1

On the errai project (root folder) run:

    $ mvn gwt:run

Although the project downloaded all dependencies and compiled successfully, it failed to start up.

This maven goal fails, since we do not have a graphical environment available in our VM.

#### 3.3) Run CA02 Part 1

In CA2 with are now using Gradle instead of maven to build projects.

3.3.1) Gradle build and extra tasks:
  
Build the gradle project with the following command:

    $ gradlew build

Run the tasks created in the class assignment:

    $ gradlew copySRC

    $ gradlew zipSRC

Both tasks executed successfully.

3.3.1) Run gradle application:

Start the gradle project with the following commands:

Server:

    $ gradlew runServer

Client:
  
 \$ gradlew runClient

Since our host machine has the graphical environment required for the chatbot client to execute, the client was run from the host machine OS.

However, to do this, the address on the host machine gradle file was changed from "localhost" to the VM address, on task "runClient":
  
 args '192.168.56.100', '59001'

The application started successfully with the server still running on the VM, and the client on the host machine:

    $ ./gradlew runClient

#### 3.3) Run CA2P2

First try to build the project:

    $ gradlew build

The following tasks were all executed successfully:

    $ gradlew renderPlantUml

    $ gradlew javadoc
