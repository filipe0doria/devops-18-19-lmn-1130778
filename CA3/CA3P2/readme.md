## CA3, Part 2 - Virtualization with Vagrant

### Step 1 - Install and Configure Vagrant

1.1) Vagrant was installed following the steps described on the class;

### Step 2 - Study the Vagrantfile

2.1) Analysis of the description of the file in:

https://github.com/atb/vagrant-multi-demo

2.2) Follow the steps on READMEmd file.

2.2.1) Create a local folder in your computer CA3P2 and copy the Vagrantfile into that folder.

Download the Vagrantfile from the URL mentioned above and copy it to the vagrant directory created previously.

### Step 3 - Build a war file

> gradle build

This generates an executable artifact of the project, which was then copied to the vagrant folder.

### Step 4 - Execute Vagrant Up

On the following folder _CA3P2/vagrant_ run:

> vagrant up

This will set up both VM according to the specification of the Vagrantfile.

### Step 5 - Test application

Confirming that VMs started as expected by accessing the urls:

- errai: http://192.168.33.10:8080/errai-demonstration-gradle/
- h2: http://192.168.33.11:8082 - H2 console db file path ="jdbc:h2:tcp://192.168.33.11:9092/~/test"

### Step 6 - Configure H2 DB

_"When you execute the application you will see that data is lost when the VMs are
terminated! This is because the Errai application is not using the H2 server from
the db VM!!_

So this mean that initially the errai application is not using the H2 server because it is configured to use a memory H2 database, we will need to updated it to use the DB create on the db VM.

**6.1) Update errai to use VM H2 vagrant db and to persist data between sessions.**

Going back to the errai project we need to update the _persistence.xml_ document which is where persistence configurations reside.

- Update errai project "persistence.xml" to use "update" instead of "create-drop" - in main/resources/META-INF/persistence.xml
  > < property name="hibernate.hbm2ddl.auto" value="update"/>

**6.2) Update remote database connection and credentials through Vagrantfile**
Before the following updates on the Vagrantfile, both machines were shutdown in order to avoid files corruption.

> vagrant halt

- Update database connection path to the remote URL, in standalone.xml.

```
#So that wildlfy writes on the remote db instead of the local in memory one
	  sed -i 's#jdbc:h2:mem:test#jdbc:h2:tcp://192.168.33.11:9092/~/test#g' wildfly-15.0.1.Final/standalone/configuration/standalone.xml
```

- Update also in standalone.xml, for the remote db credentials - in wildfly-15.0.1.Final/standalone/configuration/standalone.xml

```
#So that wildfly uses this set of user credentials to connect to the remote db
 sed -i 's#<password>sa</password>#<password></password>#g' wildfly-15.0.1.Final/standalone/configuration/standalone.xml
```

**6.3) DB data persisted even when the VM is destroyed or shutdown.**
This update was also done in the Vagrantfile

```
db.trigger.before :halt do |trigger|
    trigger.warn = "Database will be backuped to /vagrant/test.mv.db"
    trigger.run_remote = {inline: "sudo cp /root/test.mv.db /vagrant/test.mv.db"}
    end
```

Which works as expected and a new file called test.mv.db can be seen in the _web_ VM.

```
PS C:\Users\Philipe\IdeaProjects\DevOps\devops-18-19-lmn-1130778\CA3\CA3P2\vagrant> vagrant halt                                                                                                                             ==> web: Attempting graceful shutdown of VM...                                                                                                                                                                               ==> db: Running action triggers before halt ...
==> db: Running trigger...
==> db: Database will be backuped to /vagrant/test.mv.db
    db: Running: inline script
==> db: Attempting graceful shutdown of VM...
```

So that wildlfy accepts remote connections, it was also there was already in the base Vagrant file the following command which changes the wildfly configuration to use any address.

```
  sed -i 's#<inet-address value="${jboss\.bind\.address:127\.0\.0\.1}"/>#<any-address/>#g' /home/vagrant/wildfly-15.0.1.Final/standalone/configuration/standalone.xml
```

### Step 7 - Wrapping up

** Build project with gradle to re-generate war file:**

> gradle clean
> gradle build

Copy the generated war file to the vagrant folder:

> cp CA2P2/build/libs/errai-demonstration-gradle-master.war ../CA3/CA3P2/vagrant/errai-demonstration-gradle.war

Restart the VMs

> vagrant reload --provision

### Step 8 - Testing updates

Confirmed that VMs started as expected by accessing:

- errai: http://192.168.33.10:8080/errai-demonstration-gradle/
- h2: http://192.168.33.11:8082 (remote db path "jdbc:h2:tcp://192.168.33.11:9092/~/test")

Adding a new contact through the UI, it is possible to confirm both in the UI and through the h2 console menu that it was persisted and displayed correctly.

Reboting the VMs it was possible to verify the data was backup and that it was still persisted.

> vagrant halt
> vagrant up

### Step 9 - Committing and pushing the updates

The updates on the README.md, Vagrantfile and persistence.xml files were pushed to the repository:

> git status
> git add .
> git commit -m "FIX #21 - README.md, Vagrantfile, .war and persistence.xml"
> git push origin master

### Step 10 - Tag the Repository

> git tag -a CA3P2 -m "CA3P2"
> git push origin CA3P2
